package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @Getter
    @NotNull
    public final String NAME = "project-remove-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Удалить проект по Id.";


    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER ID]");
        @NotNull final String value = TerminalUtil.inLine();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(value);
        getProjectEndpoint().removeProjectById(request);

    }

}
