package ru.t1.dkononov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.services.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @Value("#{environment['server.host']}")
    public String host;

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMITER_NAME = "gitCommiterName";

    @NotNull
    public static final String GIT_COMMITER_EMAIL = "gitCommiterEmail";

    @NotNull
    public final Properties properties = new Properties();

    public PropertyService() {
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getPort() {
        return serverPort.toString();
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return read(APPLICATION_FILE_NAME_KEY);
    }

    @Override
    public @NotNull String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @Override
    public @NotNull String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @Override
    public @NotNull String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @Override
    public @NotNull String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @Override
    public @NotNull String getGitCommiterName() {
        return read(GIT_COMMITER_NAME);
    }

    @Override
    public @NotNull String getGitCommiterEmail() {
        return read(GIT_COMMITER_EMAIL);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
