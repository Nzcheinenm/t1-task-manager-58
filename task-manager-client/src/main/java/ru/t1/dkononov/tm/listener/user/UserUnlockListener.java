package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.UserUnlockRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @Getter
    @NotNull
    private final String DESCRIPTION = "user unlock";

    @Getter
    @NotNull
    private final String NAME = "user-unlock";

    @Override
    @EventListener(condition = "@userUnlockListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.inLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
