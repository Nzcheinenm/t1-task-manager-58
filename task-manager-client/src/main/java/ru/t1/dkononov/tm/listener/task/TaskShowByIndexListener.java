package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.model.TaskDTO;
import ru.t1.dkononov.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.dkononov.tm.dto.response.TaskGetByIndexResponse;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-show-by-index";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Показать задачу по индексу.";

    @Override
    @EventListener(condition = "@taskShowByIndexListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX]");
        @NotNull final Integer value = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(value);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpointClient().getTaskByIndex(request);
        if (response.getTask() == null) response.setTask(new TaskDTO());
        @NotNull final TaskDTO task = response.getTask();
        System.out.println(show(task));
    }

    @NotNull
    public String show(@NotNull final TaskDTO task) {
        return "[ID: " + task.getId() + "]\n" +
                "[NAME: " + task.getName() + "]\n" +
                "[DESC: " + task.getDescription() + "]\n" +
                "[STATUS: " + task.getStatus() + "]";
    }

}
