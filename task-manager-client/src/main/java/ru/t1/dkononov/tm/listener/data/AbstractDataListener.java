package ru.t1.dkononov.tm.listener.data;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkononov.tm.listener.AbstractListener;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    @Getter
    @Setter
    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

}
