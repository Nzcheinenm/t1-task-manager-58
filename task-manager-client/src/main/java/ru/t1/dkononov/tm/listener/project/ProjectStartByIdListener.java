package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @Getter
    @NotNull
    public final String NAME = "project-start-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Начать проект по Id.";

    @Override
    @EventListener(condition = "@projectStartByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[IN PROGRESS PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.inLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().startProjectById(request);
    }

}
