package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskStartByIndexRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-start-by-index";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Начать задачу по индексу.";

    @Override
    @EventListener(condition = "@taskStartByIndexListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[IN PROGRESS PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpointClient().startTaskByIndex(request);
    }

}
