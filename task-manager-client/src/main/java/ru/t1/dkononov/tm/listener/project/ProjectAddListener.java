package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ProjectCreateRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.field.AbstractFieldException;
import ru.t1.dkononov.tm.util.TerminalUtil;


@Component
public final class ProjectAddListener extends AbstractProjectListener {

    @Getter
    @Nullable
    public final String DESCRIPTION = "Создать новый проект.";

    @Getter
    @Nullable
    public final String NAME = "project-add";


    @Override
    @EventListener(condition = "@projectAddListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractFieldException {
        System.out.println("[CREATE NEW PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.inLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.inLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().createProject(request);
    }

}
