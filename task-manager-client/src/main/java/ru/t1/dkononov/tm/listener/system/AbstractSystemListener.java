package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkononov.tm.api.services.ICommandService;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.listener.AbstractListener;
import ru.t1.dkononov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Getter
    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
