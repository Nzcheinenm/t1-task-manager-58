package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-remove-by-index";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Удалить задачу по индексу.";

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER INDEX]");
        @NotNull final Integer value = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(value);
        getTaskEndpointClient().removeTaskByIndex(request);
    }

}
