package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskClearRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-clear";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Очистить список задач.";

    @Override
    @EventListener(condition = "@taskClearListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CLEAR LIST TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        getTaskEndpointClient().clearTask(request);
    }

}
