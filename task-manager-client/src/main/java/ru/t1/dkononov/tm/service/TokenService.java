package ru.t1.dkononov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.services.ITokenService;

@Getter
@Setter
@Service
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
