package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.dto.request.UserProfileRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @Getter
    @NotNull
    private final String NAME = "view-user-profile";

    @Getter
    @NotNull
    private final String DESCRIPTION = "view profile of current user";

    @Override
    @EventListener(condition = "@userViewProfileListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserDTO user = getAuthEndpoint().profile(request).getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getId());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
