package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-complete-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Завершить задачу по Id.";

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.inLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        getTaskEndpointClient().completeTaskById(request);
    }

}
