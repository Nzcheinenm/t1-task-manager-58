package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.UserLogoutRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @Getter
    @NotNull
    private final String NAME = "logout";

    @Getter
    @NotNull
    private final String DESCRIPTION = "logout current user";

    @Override
    @EventListener(condition = "@userLogoutListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        setToken(null);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
