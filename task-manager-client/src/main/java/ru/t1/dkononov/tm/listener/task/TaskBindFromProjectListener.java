package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskBindFromProjectListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-bind-to-project";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Привязать задачу к проекту.";

    @Override
    @EventListener(condition = "@taskBindFromProjectListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.inLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.inLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        getTaskEndpointClient().bindTaskToProject(request);
    }

}
