package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.model.TaskDTO;
import ru.t1.dkononov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.dkononov.tm.dto.response.TaskGetByIdResponse;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-show-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Показать задачу по Id.";

    @Override
    @EventListener(condition = "@taskShowByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID]");
        @NotNull final String scanner = TerminalUtil.inLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(scanner);
        @NotNull final TaskGetByIdResponse response = getTaskEndpointClient().getTaskById(request);
        if (response.getTask() == null) response.setTask(new TaskDTO());
        @NotNull final TaskDTO task = response.getTask();
        System.out.println(show(task));
    }

    @NotNull
    public String show(@NotNull final TaskDTO task) {
        return "[ID: " + task.getId() + "]\n" +
                "[NAME: " + task.getName() + "]\n" +
                "[DESC: " + task.getDescription() + "]\n" +
                "[STATUS: " + task.getStatus() + "]";
    }

}
