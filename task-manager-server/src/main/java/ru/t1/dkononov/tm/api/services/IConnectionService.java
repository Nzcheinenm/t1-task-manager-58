package ru.t1.dkononov.tm.api.services;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {


    @NotNull EntityManager getEntityManager();

    @NotNull
    @SneakyThrows
    void beforeInitLiquibase();

    @NotNull
    @SneakyThrows
    Liquibase getLiquibase();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

}
