package ru.t1.dkononov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.services.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public final static String EMPTY_VALUE = "---";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMITER_NAME = "gitCommiterName";

    @NotNull
    public static final String GIT_COMMITER_EMAIL = "gitCommiterEmail";

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private String sessionTimeout;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2auto;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactory;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseProviderConfig;

    @Value("#{environment['token.init']}")
    private String tokenInit;

    @Value("#{environment['database.liquibase_config']}")
    private String liquibaseConfig;

    @Value("#{environment['jms.endpoint']}")
    private String jmsEndpoint;

    @NotNull
    public final Properties properties = new Properties();

    public PropertyService() {
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @Override
    public @NotNull String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @Override
    public @NotNull String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @Override
    public @NotNull String getApplicationConfig() {
        return read(APPLICATION_FILE_NAME_KEY);
    }

    @Override
    public @NotNull String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @Override
    public @NotNull String getGitCommiterName() {
        return read(GIT_COMMITER_NAME);
    }

    @Override
    public @NotNull String getGitCommiterEmail() {
        return read(GIT_COMMITER_EMAIL);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }


}
