package ru.t1.dkononov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dkononov.tm.api.services.IAdminService;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.dto.request.DropSchemeRequest;
import ru.t1.dkononov.tm.dto.request.InitSchemeRequest;
import ru.t1.dkononov.tm.dto.response.SchemeDropResponse;
import ru.t1.dkononov.tm.dto.response.SchemeInitResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.IAdminEndpoint")
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    @Getter
    private IConnectionService сonnectionService;

    @NotNull
    @Autowired
    @Getter
    private IAdminService adminService;


    @NotNull
    @Override
    @WebMethod
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final DropSchemeRequest request

    ) {
        getAdminService().dropScheme(request.getToken());
        return new SchemeDropResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final InitSchemeRequest request

    ) {
        getAdminService().initScheme(request.getToken());
        return new SchemeInitResponse();
    }

}

