package ru.t1.dkononov.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.services.IAdminService;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.exception.field.AccessDeniedException;

@Service
public class AdminService implements IAdminService {

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;


    @Override
    @SneakyThrows
    public void dropScheme(@NotNull String token) {
        if (token == null || token.isEmpty() ||
                token.equals(propertyService.getTokenInit()))
            throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
    }

    @Override
    @SneakyThrows
    public void initScheme(@NotNull String token) {
        if (token == null || token.isEmpty() ||
                token.equals(propertyService.getTokenInit()))
            throw new AccessDeniedException();
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
    }

}
