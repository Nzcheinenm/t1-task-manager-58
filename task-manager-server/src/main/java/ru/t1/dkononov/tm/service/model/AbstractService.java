package ru.t1.dkononov.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.repository.model.IRepository;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.IService;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.model.AbstractModel;
import ru.t1.dkononov.tm.repository.model.AbstractRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends AbstractRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    protected ApplicationContext context;


    @NotNull
    protected abstract IRepository<M> getRepository();

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id) != null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
            result = findById(model.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models.forEach(repository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models.forEach(repository::update);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }


    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) throws UserIdEmptyException {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final Integer index)
            throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            result = repository.findById(id);
            remove(result);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            result = repository.findByIndex(index);
            repository.remove(result);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void removeAll() throws UserIdEmptyException {
        @NotNull final IRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
