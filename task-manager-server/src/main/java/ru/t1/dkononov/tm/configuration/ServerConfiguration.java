package ru.t1.dkononov.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.dto.model.ProjectDTO;
import ru.t1.dkononov.tm.dto.model.SessionDTO;
import ru.t1.dkononov.tm.dto.model.TaskDTO;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.model.Project;
import ru.t1.dkononov.tm.model.Session;
import ru.t1.dkononov.tm.model.Task;
import ru.t1.dkononov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ComponentScan("ru.t1.dkononov.tm")
public class ServerConfiguration {

    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static Database DATABASE;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    @SneakyThrows
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();

        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUser());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2auto());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLvlCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactory());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseProviderConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    private Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public EntityManager getEntityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public Liquibase getLiquibase() {
        return liquibase(propertyService.getLiquibaseConfig());
    }

    private void initLiquibase() throws IOException, SQLException, DatabaseException {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = getConnection(properties);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @SneakyThrows
    public Liquibase liquibase(final String filename) {
        initLiquibase();
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }


}
