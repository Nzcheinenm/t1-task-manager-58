package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.service.PropertyService;

import static ru.t1.dkononov.tm.constant.TestData.*;

@Category(DataCategory.class)
public class ProjectRepositoryTest extends AbstractSchemaTest {

    @NotNull
    private final ProjectDTORepository repository = context.getBean(ProjectDTORepository.class);

    @BeforeClass
    public static void before() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
    }

    @Before
    public void init() {
        try {
            @NotNull final UserDTORepository userDTORepository = context.getBean(UserDTORepository.class);
            entityManager.getTransaction().begin();
            userDTORepository.add(USER1);
            repository.add(USER1.getId(), USER_PROJECT);
            repository.add(USER1.getId(), USER_PROJECT2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        try {
            entityManager.getTransaction().begin();
            repository.clear(USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        try {
            @NotNull final ProjectDTORepository repository = context.getBean(ProjectDTORepository.class);
            entityManager.getTransaction().begin();
            repository.add(USER_PROJECT);
            Assert.assertEquals(USER_PROJECT.getUserId(), USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() {
        try {
            @NotNull final ProjectDTORepository repository = context.getBean(ProjectDTORepository.class);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_PROJECT);
            Assert.assertEquals(USER_PROJECT, repository.findById(USER_PROJECT.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findById() {
        try {
            @NotNull final ProjectDTORepository repository = context.getBean(ProjectDTORepository.class);
            Assert.assertNotNull(repository.findById(USER1.getId(), USER_PROJECT.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAll() {
        try {
            @NotNull final ProjectDTORepository repository = context.getBean(ProjectDTORepository.class);
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
